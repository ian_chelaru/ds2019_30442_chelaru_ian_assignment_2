import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap'
import { Link } from 'react-router-dom';

class MedicationList extends Component {

    emptyMedication = {
        id: ''
    };

    constructor(props) {
        super(props);
        this.state = { medications: [], isLoading: true, medicationToDelete: this.emptyMedication };
        this.remove = this.remove.bind(this);
    }

    componentDidMount() {
        this.setState({ isLoading: true });

        fetch('/api/medications')
            .then(response => response.json())
            .then(data => this.setState({ medications: data, isLoading: false }));
    }

    async remove(id) {
        let medication = { ...this.state.emptyMedication };
        medication['id'] = id;
        this.setState({ medicationToDelete: medication });
        await fetch('/api/medications', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(medication),
        }).then(() => {
            let updatedMedications = [...this.state.medications].filter(i => i.id !== id);
            this.setState({ medications: updatedMedications });
        });
    }

    render() {
        const { medications, isLoading } = this.state;

        if (isLoading) {
            return <p>Loading...</p>;
        }

        const medicationList = medications.map(medication => {
            return <tr key={medication.id}>
                <td>{medication.name}</td>
                <td>{medication.sideEffects}</td>
                <td>
                    <ButtonGroup>
                        <Button size="sm" color="primary" tag={Link} to={"/medications/" + medication.id}>Edit</Button>
                        <Button size="sm" color="danger" onClick={() => this.remove(medication.id)}>Delete</Button>
                    </ButtonGroup>
                </td>
            </tr>
        });

        return (
            <div>
                <Container fluid>
                    <div className="float-right">
                        <Button color="success" tag={Link} to={"medications/new/"}>Add Medication</Button>
                    </div>
                    <h3>Medications</h3>
                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Side Effects</th>
                            </tr>
                        </thead>
                        <tbody>
                            {medicationList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default MedicationList;