import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Container, Form, FormGroup, Label, Input, Button } from 'reactstrap';

class MedicationEdit extends Component {

    emptyMedication = {
        name: '',
        sideEffects: ''
    };

    constructor(props) {
        super(props);
        this.state = { medication: this.emptyMedication };
        this.submitHandler = this.submitHandler.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }

    async componentDidMount() {
        const medicationId = this.props.match.params.id;
        if (medicationId !== 'new') {
            const medication = await (await fetch(`/api/medications/${medicationId}`)).json();
            this.setState({ medication: medication });
        }
    }

    async submitHandler(event) {
        event.preventDefault();
        const { medication } = this.state;
        await fetch('/api/medications', {
            method: (medication.id) ? "PUT" : "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(medication)
        });
        this.props.history.push('/medications');
    }

    changeHandler(event) {
        const nam = event.target.name;
        const val = event.target.value;
        let newMedication = { ...this.state.medication };
        newMedication[nam] = val;
        this.setState({ medication: newMedication });
    }

    render() {
        const { medication } = this.state;
        const title = medication.id ? 'Edit Medication' : 'Add Medication';

        return <div>
            <Container>
                <h2>{title}</h2>
                <Form onSubmit={this.submitHandler}>
                    <FormGroup>
                        <Label for="name">Name</Label>
                        <Input type="text" id="name" name="name" value={medication.name || ''} onChange={this.changeHandler} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="side_effects">Side Effects</Label>
                        <Input type="text" id="side_effects" name="sideEffects" value={medication.sideEffects || ''} onChange={this.changeHandler}></Input>
                    </FormGroup>
                    <FormGroup>
                        <Button type="submit" color="primary">Save</Button>{' '}
                        <Button color="secondary" tag={Link} to="/medications">Cancel</Button>
                    </FormGroup>
                </Form>
            </Container>
        </div>
    }
}

export default withRouter(MedicationEdit);