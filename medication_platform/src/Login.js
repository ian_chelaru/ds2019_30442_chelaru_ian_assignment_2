import React, { Component } from 'react';
import './Login.css'
import { withRouter } from 'react-router-dom';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { username: '', password: '' }
        this.submitHandler = this.submitHandler.bind(this);
    }

    async submitHandler(event) {
        event.preventDefault();
        let username = this.state.username;
        let password = this.state.password;
        let user = await (await fetch(`/api/users/${username}`)).json();
        if (password === user.password) {
            switch (user.role) {
                case "DOCTOR":
                    this.props.history.push('/doctor');
                    break;
                case "CAREGIVER":
                    this.props.history.push({ pathname: '/caregiver', state: { user: user } }
                    );
                    break;
                default:
                    this.props.history.push({ pathname: '/patient', state: { user: user } });
            }
        }
        else {
            alert("Invalid password");
        }
    }

    changeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({ [nam]: val });
    }

    render() {
        return <div>
            <div className="login-form">
                <form onSubmit={this.submitHandler}>
                    <h2 className="text-center">Log in</h2>
                    <div className="form-group">
                        <input onChange={this.changeHandler} name="username" type="text" className="form-control" placeholder="Username" required="required" />
                    </div>
                    <div className="form-group">
                        <input onChange={this.changeHandler} name="password" type="password" className="form-control" placeholder="Password" required="required" />
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary btn-block">Log in</button>
                    </div>
                </form>
            </div>
        </div>
    }
}

export default withRouter(Login);