import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Container, Form, FormGroup, Label, Input, Button } from 'reactstrap';

class PatientEdit extends Component {

    emptyPatient = {
        name: '',
        birthDate: new Date(),
        gender: '',
        address: ''
    };

    constructor(props) {
        super(props);
        this.state = { patient: this.emptyPatient };
        this.submitHandler = this.submitHandler.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }

    async componentDidMount() {
        const patientId = this.props.match.params.id;
        if (patientId !== 'new') {
            const patient = await (await fetch(`/api/patients/${patientId}`)).json();
            this.setState({ patient: patient });
        }
    }

    async submitHandler(event) {
        event.preventDefault();
        const { patient } = this.state;
        await fetch('/api/patients', {
            method: (patient.id) ? "PUT" : "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(patient)
        });
        this.props.history.push('/patients');
    }

    changeHandler(event) {
        const nam = event.target.name;
        const val = event.target.value;
        let newPatient = { ...this.state.patient };
        newPatient[nam] = val;
        this.setState({ patient: newPatient });
    }

    render() {
        const { patient } = this.state;
        const title = patient.id ? 'Edit Patient' : 'Add Patient';

        return <div>
            <Container>
                <h2>{title}</h2>
                <Form onSubmit={this.submitHandler}>
                    <FormGroup>
                        <Label for="name">Name</Label>
                        <Input type="text" id="name" name="name" value={patient.name || ''} onChange={this.changeHandler} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="birth_date">Birth Date</Label>
                        <Input type="date" id="birth_date" name="birthDate" value={patient.birthDate || ''} onChange={this.changeHandler} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="gender">Gender</Label>
                        <Input type="text" id="gender" name="gender" value={patient.gender || ''} onChange={this.changeHandler}></Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for="address">Address</Label>
                        <Input type="text" id="address" name="address" value={patient.address || ''} onChange={this.changeHandler}></Input>
                    </FormGroup>
                    <FormGroup>
                        <Button type="submit" color="primary">Save</Button>{' '}
                        <Button color="secondary" tag={Link} to="/patients">Cancel</Button>
                    </FormGroup>
                </Form>
            </Container>
        </div>
    }
}

export default withRouter(PatientEdit);