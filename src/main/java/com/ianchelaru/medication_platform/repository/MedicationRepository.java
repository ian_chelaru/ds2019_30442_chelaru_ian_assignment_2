package com.ianchelaru.medication_platform.repository;

import com.ianchelaru.medication_platform.enitities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Integer>
{
    @Query(value = "SELECT m FROM Medication m ORDER BY m.name")
    List<Medication> getAllOrderedByName();

    Optional<Medication> findByName(String name);
}
