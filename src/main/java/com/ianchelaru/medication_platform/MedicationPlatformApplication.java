package com.ianchelaru.medication_platform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class MedicationPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicationPlatformApplication.class, args);
	}

}
