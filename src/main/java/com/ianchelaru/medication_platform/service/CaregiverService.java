package com.ianchelaru.medication_platform.service;

import com.ianchelaru.medication_platform.enitities.Caregiver;
import com.ianchelaru.medication_platform.errorhandler.ResourceNotFoundException;
import com.ianchelaru.medication_platform.repository.CaregiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaregiverService
{
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository)
    {
        this.caregiverRepository = caregiverRepository;
    }

    public Integer insert(Caregiver caregiver)
    {
        return caregiverRepository.save(caregiver).getId();
    }

    public Caregiver findById(Integer id)
    {
        return caregiverRepository.findById(id).
                orElseThrow(() -> new ResourceNotFoundException("Caregiver","id",id));
    }

    public List<Caregiver> findAll()
    {
        return caregiverRepository.findByOrderByNameAsc();
    }

    public Integer update(Caregiver caregiver)
    {
        caregiverRepository.findById(caregiver.getId()).orElseThrow(
                () -> new ResourceNotFoundException("Caregiver","id",caregiver.getId()));
        return caregiverRepository.save(caregiver).getId();
    }

    public void delete(Caregiver caregiver)
    {
        caregiverRepository.deleteById(caregiver.getId());
    }

}
