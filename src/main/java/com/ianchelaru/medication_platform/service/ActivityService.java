package com.ianchelaru.medication_platform.service;

import com.ianchelaru.medication_platform.dto.ActivityDTO;
import com.ianchelaru.medication_platform.enitities.Activity;
import com.ianchelaru.medication_platform.enitities.Patient;
import com.ianchelaru.medication_platform.repository.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityService {
    private final ActivityRepository activityRepository;
    private final PatientService patientService;

    @Autowired
    public ActivityService(ActivityRepository activityRepository, PatientService patientService) {
        this.activityRepository = activityRepository;
        this.patientService = patientService;
    }

    public Integer insert(ActivityDTO activityDTO) {
        Patient patient = patientService.findById(activityDTO.getPatientId());
        Activity activity = new Activity(patient, activityDTO.getName(), activityDTO.getStart(), activityDTO.getEnd());
        return activityRepository.save(activity).getId();
    }
}
