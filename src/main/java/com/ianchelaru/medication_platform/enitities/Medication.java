package com.ianchelaru.medication_platform.enitities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Getter
@Setter
public class Medication
{
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String name;

    @Column
    private String sideEffects;

    @OneToMany(mappedBy = "medication")
    @JsonIgnore
    private Set<Prescription> prescriptionSet;
}
