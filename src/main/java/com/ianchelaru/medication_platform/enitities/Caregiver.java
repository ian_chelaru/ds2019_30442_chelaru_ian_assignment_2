package com.ianchelaru.medication_platform.enitities;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Caregiver
{
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column
    private Date birthDate;

    @Column(nullable = false)
    private String gender;

    @Column
    private String address;

    @OneToMany(mappedBy = "caregiver")
    private Set<Patient> patients;

//    public Caregiver(String name,String gender,String address)
//    {
//        this.name = name;
//        this.gender = gender;
//        this.address = address;
//    }
}
