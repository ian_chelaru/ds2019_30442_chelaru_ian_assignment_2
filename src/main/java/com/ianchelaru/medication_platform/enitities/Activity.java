package com.ianchelaru.medication_platform.enitities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

import static javax.persistence.GenerationType.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Activity {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "patient_id", nullable = false)
    @JsonIgnore
    private Patient patient;

    @Column(name = "activity", nullable = false)
    private String name;

    @Column(nullable = false)
    private Long start;

    @Column(nullable = false)
    private Long end;

    public Activity(Patient patient, String name, Long start, Long end) {
        this.patient = patient;
        this.name = name;
        this.start = start;
        this.end = end;
    }
}
