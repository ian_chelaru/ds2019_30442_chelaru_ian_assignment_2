package com.ianchelaru.medication_platform.consumer;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class ActivityPostRequest {
    private static final String URL_STRING = "http://localhost:8080/api/activities";
    private static final String URL_NOTIFY = "http://localhost:8080/api/notifications";

    public static void postActivity(String jsonActivity) throws IOException
    {
        sendRequest(jsonActivity, URL_STRING);
    }

    public static void notifyCaregiver(String jsonActivity) throws IOException
    {
        sendRequest(jsonActivity, URL_NOTIFY);
    }

    private static void sendRequest(String jsonActivity, String url) throws IOException {
        StringEntity entity = new StringEntity(jsonActivity, ContentType.APPLICATION_JSON);
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(url);
        request.setEntity(entity);
        HttpResponse response = httpClient.execute(request);
        System.out.println(response.getStatusLine().getStatusCode());
        System.out.println("------------------------------------------");

//        URL url = new URL(URL_STRING);
//        HttpURLConnection con = (HttpURLConnection) url.openConnection();
//        con.setRequestMethod("POST");
//        con.setRequestProperty("Content-Type", "application/json; utf-8");
//        con.setRequestProperty("Accept", "applications/json");
//        con.setDoOutput(true);
//        try (OutputStream os = con.getOutputStream()) {
//            byte[] body = jsonActivity.getBytes(StandardCharsets.UTF_8);
//            os.write(body, 0, body.length);
//            os.flush();
//        }
    }
}
