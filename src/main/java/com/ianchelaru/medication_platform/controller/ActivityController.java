package com.ianchelaru.medication_platform.controller;

import com.ianchelaru.medication_platform.dto.ActivityDTO;
import com.ianchelaru.medication_platform.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/activities")
public class ActivityController
{
    private final ActivityService activityService;

    @Autowired
    public ActivityController(ActivityService activityService,SimpMessagingTemplate template)
    {
        this.activityService = activityService;
    }

    @PostMapping
    public Integer insert(@RequestBody ActivityDTO activityDTO)
    {
        return activityService.insert(activityDTO);
    }
}
