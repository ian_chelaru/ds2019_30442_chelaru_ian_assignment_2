package com.ianchelaru.medication_platform.controller;

import com.ianchelaru.medication_platform.dto.ActivityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/notifications")
public class NotificationController
{
    private final SimpMessagingTemplate template;

    @Autowired
    public NotificationController(SimpMessagingTemplate template)
    {
        this.template = template;
    }

    @PostMapping
    public void notify(@RequestBody ActivityDTO activityDTO)
    {
        template.convertAndSend("/topic/notify", activityDTO);
    }
}
