package com.ianchelaru.medication_platform.producer;

import javax.json.Json;
import javax.json.JsonObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class Data {
    private static final Integer PATIENT_ID = 1;

    private List<JsonObject> activityList;

    Data() {
        try {
            this.activityList = this.gatherData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    List<JsonObject> getActivityList() {
        return activityList;
    }

    private List<JsonObject> gatherData() throws IOException {
        activityList = new ArrayList<>();

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("activity.txt");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (inputStream != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            while (reader.ready()) {
                String line = reader.readLine();
                Date startDate = new Date();
                Date endDate = new Date();
                try {
                    startDate = format.parse(line.substring(0, 19));
                    endDate = format.parse(line.substring(21, 40));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String activityName = line.substring(42).trim();
                JsonObject activity = Json.createObjectBuilder()
                        .add("patient_id", PATIENT_ID)
                        .add("activity", activityName)
                        .add("start", startDate.getTime())
                        .add("end", endDate.getTime())
                        .build();

                activityList.add(activity);
            }
        }
        return activityList;
    }
}
