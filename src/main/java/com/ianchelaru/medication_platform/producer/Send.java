package com.ianchelaru.medication_platform.producer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import javax.json.JsonObject;
import java.util.List;

public class Send {
    private final static String QUEUE_NAME = "activities";

    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            List<JsonObject> activityList = new Data().getActivityList();
            for (JsonObject activity : activityList)
            {
                channel.basicPublish("", QUEUE_NAME, null, activity.toString().getBytes());
                System.out.println(" [x] Sent '" + activity.toString() + "'");
                Thread.sleep(1000);
            }
        }
    }
}
